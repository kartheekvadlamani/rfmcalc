import com.datastax.spark.connector.ColumnRef;
import com.datastax.spark.connector.cql.TableDef;
import com.datastax.spark.connector.writer.RowWriter;
import com.datastax.spark.connector.writer.RowWriterFactory;
import scala.collection.IndexedSeq;

import java.io.Serializable;

/**
 * Created by kartheekVadlamani on 8/25/16.
 */
public class CustomerWriterFactory implements RowWriterFactory<Customer>, Serializable {
    private static final CustomerWriter rowWriter = new CustomerWriter();

    public RowWriter<Customer> rowWriter(TableDef tableDef, IndexedSeq<ColumnRef> columns) {
        return rowWriter;
    }
}


