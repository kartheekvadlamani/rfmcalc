import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolVersion;
import com.datastax.spark.connector.writer.RowWriter;
import scala.collection.JavaConversions;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Seq;
import java.util.Arrays;

/**
 * Created by kartheekVadlamani on 5/12/16.
 */
public class RFMDataRowWriter implements RowWriter<RFMData> {
    // Return the list of column names in the Cassandra table
    @Override
    public Seq<String> columnNames() {
        return toIndexedSeq(
                Arrays.asList("application_id", "id", "customer_id", "customer_name", "company_name", "customer_group",
                              "customer_city", "customer_state", "customer_country", "customer_email",
                              "orders_sub_total", "orders_count", "first_order_date", "last_order_date",
                              "average_days_between_orders", "first_order_amount", "last_order_amount", "average_order_price", "customer_created_at"));
    }

    public int estimateSizeInBytes(RFMData rfmData) {
        return 100;
    }

    public BoundStatement bind(RFMData rfmData, PreparedStatement statement, ProtocolVersion version) {
        BoundStatement boundStatement = new BoundStatement(statement);

        boundStatement
                .bind(rfmData.getApplicationId(), rfmData.getId(), rfmData.getCustomerId(), rfmData.getCustomerName(),
                      rfmData.getCompanyName(), rfmData.getCustomerGroup(), rfmData.getCustomerCity(),
                      rfmData.getCustomerState(), rfmData.getCustomerCountry(), rfmData.getCustomerEmail(),
                      rfmData.getOrdersSubTotal(), rfmData.getOrderCount(), rfmData.getFirstOrderDate(), rfmData.getLastOrderDate(), rfmData.getAverageDaysBetweenOrders(), rfmData.getFirstOrderAmount(), rfmData.getLastOrderAmount(), rfmData.getAverageOrderPrice(), rfmData.getCustomerCreatedAt());

        return boundStatement;
    }

    private <T> IndexedSeq<T> toIndexedSeq(Iterable<T> iterable) {
        return JavaConversions.asScalaIterable(iterable).toIndexedSeq();
    }

    @Override
    public void readColumnValues(RFMData rfmData, Object[] buffer) {
        buffer[0] = rfmData.getApplicationId();
        buffer[1] = rfmData.getId();
        buffer[2] = rfmData.getCustomerId();
        buffer[3] = rfmData.getCustomerName();
        buffer[4] = rfmData.getCompanyName();
        buffer[5] = rfmData.getCustomerGroup();
        buffer[6] = rfmData.getCustomerCity();
        buffer[7] = rfmData.getCustomerState();
        buffer[8] = rfmData.getCustomerCountry();
        buffer[9] = rfmData.getCustomerEmail();
        buffer[10] = rfmData.getOrdersSubTotal();
        buffer[11] = rfmData.getOrderCount();
        buffer[12] = rfmData.getFirstOrderDate();
        buffer[13] = rfmData.getLastOrderDate();
        buffer[14] = rfmData.getAverageDaysBetweenOrders();
        buffer[15] = rfmData.getFirstOrderAmount();
        buffer[16] = rfmData.getLastOrderAmount();
        buffer[17] = rfmData.getAverageOrderPrice();
        buffer[18] = rfmData.getCustomerCreatedAt();
    }
}
