import com.datastax.spark.connector.ColumnRef;
import com.datastax.spark.connector.cql.TableDef;
import com.datastax.spark.connector.writer.RowWriter;
import com.datastax.spark.connector.writer.RowWriterFactory;
import scala.collection.IndexedSeq;

import java.io.Serializable;

/**
 * Created by kartheekVadlamani on 5/18/16.
 */
public class RFMSummaryStatisticsWriterFactory implements RowWriterFactory<RFMSummaryStatistics>, Serializable {
    private static final RFMSummaryStatisticsWriter rowWriter = new RFMSummaryStatisticsWriter();

    public RowWriter<RFMSummaryStatistics> rowWriter(TableDef tableDef, IndexedSeq<ColumnRef> columns) {
        return rowWriter;
    }
}

