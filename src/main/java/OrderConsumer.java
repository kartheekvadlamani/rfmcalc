/**
 * Created by kartheekVadlamani on 8/24/16.
 */

import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;
import org.apache.avro.generic.IndexedRecord;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.datastax.spark.connector.japi.CassandraJavaUtil.javaFunctions;

public class OrderConsumer implements Runnable {
    private KafkaStream _kafkaStream;
    private JavaSparkContext _sparkContext;
    private Logger _logger;
    private String _cassandraSchemaName;
    private String _cassandraTableName;

    public OrderConsumer(KafkaStream kafkaStream, JavaSparkContext sparkContext, Logger logger, String cassandraSchemaName, String cassandraTableName) {
        _kafkaStream = kafkaStream;
        _sparkContext = sparkContext;
        _logger = logger;
        _cassandraSchemaName = cassandraSchemaName;
        _cassandraTableName = cassandraTableName;
    }

    public void run() {
        ConsumerIterator it = _kafkaStream.iterator();

        while (it.hasNext()) {
            Order order = parseOrderFromStream(it);

            if (order != null) {
                List<Order> orders = new ArrayList<Order>();
                orders.add(order);

                JavaRDD<Order> ordersRdd = _sparkContext.parallelize(orders);

                // Save to Cassandra
                javaFunctions(ordersRdd).writerBuilder(_cassandraSchemaName, _cassandraTableName, new OrderWriterFactory()).saveToCassandra();

                _logger.debug("order: " + order.toString());
            }
        }
    }

    private Order parseOrderFromStream(ConsumerIterator iterator) {
        try {
            MessageAndMetadata messageAndMetadata = iterator.next();
            String key = (String) messageAndMetadata.key();
            IndexedRecord value = (IndexedRecord) messageAndMetadata.message();

            Integer id = (value.get(0) != null) ? RFMHelpers.TryParseInteger(value.get(0).toString()) : null;
            Integer applicationId = (value.get(1) != null) ? RFMHelpers.TryParseInteger(value.get(1).toString()) : null;
            String orderId = (value.get(2) != null) ? value.get(2).toString() : null;
            String orderNumber = (value.get(3) != null) ? value.get(3).toString() : null;
            String status = (value.get(4) != null) ? value.get(4).toString() : null;
            String financialStatus = (value.get(5) != null) ? value.get(5).toString() : null;
            Boolean customerIsGuest = (value.get(6) != null) ? ((Integer) value.get(6) != 0) : null;
            BigInteger customerId = (value.get(7) != null) ? RFMHelpers.TryParseBigInteger(value.get(7).toString()) : null;
            String firstName = (value.get(8) != null) ? value.get(8).toString() : null;
            String lastName = (value.get(9) != null) ? value.get(9).toString() : null;
            String email = (value.get(10) != null) ? value.get(10).toString() : null;
            BigDecimal subTotalPrice = (value.get(11) != null) ? RFMHelpers.AvroHeapByteBufferToBigDecimal(value.get(11)) : null;
            BigDecimal totalDiscounts = (value.get(12) != null) ? RFMHelpers.AvroHeapByteBufferToBigDecimal(value.get(12)) : null;
            BigDecimal storeCredit = (value.get(13) != null) ? RFMHelpers.TryParseBigDecimal(value.get(13).toString()) : null;
            BigDecimal totalPrice = (value.get(14) != null) ? RFMHelpers.AvroHeapByteBufferToBigDecimal(value.get(14)) : null;
            String currencyCode = (value.get(15) != null) ? value.get(15).toString() : null;
            String source = (value.get(16) != null) ? value.get(16).toString() : null;
            Date createdAt = (value.get(17) != null) ? new Date((Long) value.get(17)) : null;
            Date updatedAt = (value.get(18) != null) ? new Date((Long) value.get(18)) : null;
            Date customerCreatedAt = (value.get(19) != null) ? new Date((Long) value.get(19)) : null;
            Boolean isSynced = (value.get(20) != null) ? ((Integer) value.get(20) != 0) : null;
            String billingAddressId = (value.get(21) != null) ? value.get(21).toString() : null;
            String shippingAddressId = (value.get(22) != null) ? value.get(22).toString() : null;
            Date created = (value.get(23) != null) ? new Date((Long) value.get(23)) : null;
            Date modified = (value.get(24) != null) ? new Date((Long) value.get(24)) : null;
            Integer consumerOrderId = (value.get(25) != null) ? RFMHelpers.TryParseInteger(value.get(25).toString()) : null;
            String previousStatus = (value.get(26) != null) ? value.get(26).toString() : null;
            Boolean isPartialData = (value.get(27) != null) ? ((Integer) value.get(27) != 0) : null;

            Order order = new Order(id, applicationId, orderId, orderNumber, status, financialStatus, customerIsGuest, customerId, firstName, lastName, email, subTotalPrice, totalDiscounts, storeCredit, totalPrice, currencyCode, source, createdAt, updatedAt, customerCreatedAt,
                    isSynced, billingAddressId, shippingAddressId, created, modified, consumerOrderId, previousStatus, isPartialData);

            return order;
        }
        catch (SerializationException ex) {
            _logger.error(String.format("SerializationException: %s", ex.getMessage()));
            return null;
        }
    }
}



