import org.junit.Test;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;

/**
 * Created by kartheekVadlamani on 7/27/16.
 */
public class RFMHelpersTest {
    @Test
    public void testGetFormattedDate() throws Exception {
        // Arrange
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        SimpleDateFormat outputDateFormatter = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        String expectedFormattedDate = outputDateFormatter.format(today);

        // Act
        String formattedDate = RFMHelpers.GetFormattedDate(today);

        // Assert
        assertEquals(expectedFormattedDate, formattedDate);
    }

    @Test
    public void testGetFormattedFullName() throws Exception {
        // Arrange
        String firstName = "john";
        String lastName = "doe";
        String expectedFullName = "John Doe";

        // Act
        String fullName = RFMHelpers.GetFormattedFullName(firstName, lastName);

        // Assert
        assertEquals(expectedFullName, fullName);
    }

    @Test
    public void testToPascalCase() throws Exception {
        // Arrange
        String firstNamePascal = "john";
        String lastNamePascal = "doe";
        String expectedFullNamePascal = "John Doe";

        // Act
        String FullNamePascal =  RFMHelpers.ToPascalCase(firstNamePascal) + ' ' + RFMHelpers.ToPascalCase(lastNamePascal);

        // Assert
        assertEquals(expectedFullNamePascal, FullNamePascal);
    }

    @Test
    public void testGetLastOrderAmountTrue() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate1 = sdf.parse("2016-07-31 23:23:23");
        Date CreateDate2 = sdf.parse("2016-08-01 23:23:23");

        BigDecimal OrderAmount1 = new BigDecimal(100.00);
        BigDecimal OrderAmount2 = new BigDecimal(150.00);

        // Act
        BigDecimal LastOrderAmount = RFMHelpers.GetLastOrderAmount(CreateDate1, CreateDate2, OrderAmount1, OrderAmount2);

        // Assert
        assertEquals(OrderAmount2, LastOrderAmount);
    }
    @Test
    public void testGetLastOrderAmountFalse() throws Exception {
        // Arrange
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate3 = sdf1.parse("2016-08-01 23:23:23");
        Date CreateDate4 = sdf1.parse("2016-07-31 23:23:23");

        BigDecimal OrderAmount3 = new BigDecimal(125.00);
        BigDecimal OrderAmount4 = new BigDecimal(150.00);

        // Act
        BigDecimal LastOrderAmountFalse = RFMHelpers.GetLastOrderAmount(CreateDate3, CreateDate4, OrderAmount3, OrderAmount4);

        // Assert
        assertEquals(OrderAmount3, LastOrderAmountFalse);
    }

    @Test
    public void testGetFirstOrderAmountTrue() throws Exception {
        // Arrange
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate5 = sdf2.parse("2016-07-31 23:23:23");
        Date CreateDate6 = sdf2.parse("2016-08-31 23:23:23");

        BigDecimal OrderAmount5 = new BigDecimal(125.00);
        BigDecimal OrderAmount6 = new BigDecimal(150.00);

        // Act
        BigDecimal firstOrderAmountTrue = RFMHelpers.GetFirstOrderAmount(CreateDate5, CreateDate6, OrderAmount5, OrderAmount6);


        // Assert
        assertEquals(OrderAmount5, firstOrderAmountTrue);

    }

    @Test
    public void testGetFirstOrderAmountFalse() throws Exception {
        // Arrange
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf3.parse("2016-08-31 23:23:23");
        Date CreateDate8 = sdf3.parse("2016-07-31 23:23:23");

        BigDecimal OrderAmount7 = new BigDecimal(120.00);
        BigDecimal OrderAmount8 = new BigDecimal(150.00);

        // Act
        BigDecimal firstOrderAmountFalse = RFMHelpers.GetFirstOrderAmount(CreateDate7, CreateDate8, OrderAmount7, OrderAmount8);

        // Assert
        assertEquals(OrderAmount8, firstOrderAmountFalse);
    }

    @Test
    public void testGetAverageDaysBetweenOrders() throws Exception {
        // Arrange
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date firstOrderDate = sdf3.parse("2016-08-01 16:16:16");
        Date lastOrderDate = sdf3.parse("2016-08-31 16:16:16");

        Integer OrderCount = 11;
        Integer expectedResult = 3;

        // Act
        Integer actualAverageDays = RFMHelpers.GetAverageDaysBetweenOrders(firstOrderDate, lastOrderDate, OrderCount);

        // Assert
        assertEquals(actualAverageDays, expectedResult);
    }

    @Test
    public void testGetAverageDaysBetweenOrdersNull() throws Exception {
        // Arrange
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date firstOrderDate = null;
        Date lastOrderDate = sdf3.parse("2016-08-31 16:16:16");

        Integer OrderCount = 11;

        Integer expectedResult = null;
        // Act

        Integer actualAverageDays = RFMHelpers.GetAverageDaysBetweenOrders(firstOrderDate, lastOrderDate, OrderCount);

        // Assert
        assertEquals(actualAverageDays, expectedResult);
    }

    @Test
    public void testGetAverageDaysBetweenOrdersNullCase2() throws Exception {
        // Arrange
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date firstOrderDate = sdf3.parse("2016-08-01 16:16:16");
        Date lastOrderDate = sdf3.parse("2016-08-31 16:16:16");

        Integer OrderCount = null;
        Integer expectedResult = null;

        // Act
        Integer actualAverageDays = RFMHelpers.GetAverageDaysBetweenOrders(firstOrderDate, lastOrderDate, OrderCount);

        // Assert
        assertEquals(actualAverageDays, expectedResult);
    }

    @Test
    public void testGetLastOrderDate() throws Exception {
        // Arrange
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf4.parse("2016-08-31 23:23:23");
        Date CreateDate8 = sdf4.parse("2016-07-31 23:23:23");

        // Act
        Date actualDate = RFMHelpers.GetLastOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate7);
    }
    @Test
    public void testGetLastOrderDateElse() throws Exception {
        // Arrange
        SimpleDateFormat sdf6 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf6.parse("2016-07-31 23:23:2");
        Date CreateDate8 = sdf6.parse("2016-08-31 23:23:23");

        // Act
        Date actualDate = RFMHelpers.GetLastOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate8);
    }

    @Test
    public void testGetLastOrderDateNull() throws Exception {
        // Arrange
        SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = null;
        Date CreateDate8 = sdf5.parse("2016-07-31 23:23:20");

        // Act
        Date actualDate = RFMHelpers.GetLastOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate8);
    }

    @Test
    public void testGetLastOrderDateNullElse() throws Exception {
        // Arrange
        SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf5.parse("2016-07-31 23:23:20");
        Date CreateDate8 = null;

        // Act
        Date actualDate = RFMHelpers.GetLastOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate7);
    }

    @Test
    public void testGetFirstOrderDate() throws Exception {
        // Arrange
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf4.parse("2016-07-31 23:23:23");
        Date CreateDate8 = sdf4.parse("2016-08-31 23:23:23");

        // Act
        Date actualDate = RFMHelpers.GetFirstOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate7);
    }


    @Test
    public void testGetFirstOrderDateElse() throws Exception {
        // Arrange
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf4.parse("2016-08-31 23:23:23");
        Date CreateDate8 = sdf4.parse("2016-07-31 23:23:23");

        // Act
        Date actualDate = RFMHelpers.GetFirstOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate8);
    }

    @Test
    public void testGetFirstOrderDateNull() throws Exception {
        // Arrange
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = null;
        Date CreateDate8 = sdf4.parse("2016-07-31 23:23:23");

        // Act
        Date actualDate = RFMHelpers.GetFirstOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate8);
    }

    @Test
    public void testGetFirstOrderDateNullElse() throws Exception {
        // Arrange
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date CreateDate7 = sdf4.parse("2016-07-31 23:23:23");
        Date CreateDate8 = null;

        // Act
        Date actualDate = RFMHelpers.GetFirstOrderDate(CreateDate7, CreateDate8);

        // Assert
        assertEquals(actualDate, CreateDate7);
    }

    @Test
    public void testGetTotalSum() throws Exception {
        // Arrange
        BigDecimal total1 = new BigDecimal(100.00);
        BigDecimal total2 = new BigDecimal(150.00);
        BigDecimal expectedResult = total1.add(total2);

        // Act
        BigDecimal actualSum = RFMHelpers.GetTotalSum(total1, total2);

        // Assert
        assertEquals(expectedResult, actualSum);
    }

    @Test
    public void testGetTotalSumCase1() throws Exception {
        // Arrange
        BigDecimal total1 = null;
        BigDecimal total2 = new BigDecimal(150.00);

        // Act
        BigDecimal actualSum = RFMHelpers.GetTotalSum(total1, total2);

        // Assert
        assertEquals(total2, actualSum);
    }

    @Test
    public void testGetTotalSumCase2() throws Exception {
        // Arrange
        BigDecimal total1 = new BigDecimal(150.00);
        BigDecimal total2 = null;

        // Act
        BigDecimal actualSum = RFMHelpers.GetTotalSum(total1, total2);

        // Assert
        assertEquals(total1, actualSum);
    }

    @Test
    public void testGetLatestCustomerAddress() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createDate1= sdf.parse("2016-07-31 23:23:23");
        Date createDate2= sdf.parse("2016-08-31 23:23:23");
        Date createDate4= sdf.parse("2016-09-31 23:23:23");
        CustomerAddress customerAddress1 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Cooper", "Revenue Conduit", null, null,
                                    null, null, null, null, null, null, null, null, createDate1, createDate2);
        CustomerAddress customerAddress2 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Copper", "Revenue Conduit", "526 S Main",
                                    "Akron Business Accelerator", "Akron", "ohio", "123", "1", "office", "44325",
                                    "1234567809", null, createDate1, createDate4);

        // Act
        CustomerAddress actualResult = RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);

        // Assert
        assertEquals(customerAddress2, actualResult);
    }

    @Test
    public void testGetLatestCustomerAddress1() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createDate1= sdf.parse("2016-09-31 23:23:23");
        Date createDate2= sdf.parse("2016-09-31 23:23:23");
        Date createDate3= sdf.parse("2016-07-31 23:23:23");
        Date createDate4= sdf.parse("2016-08-31 23:23:23");
        CustomerAddress customerAddress1 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Cooper", "Revenue Conduit", null, null,
                                    null, null, null, null, null, null, null, null, createDate1, createDate2);
        CustomerAddress customerAddress2 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Copper", "Revenue Conduit", "526 S Main",
                                    "Akron Business Accelerator", "Akron", "ohio", "123", "1", "office", "44325",
                                    "1234567809", null, createDate3, createDate4);

        // Act
        CustomerAddress actualResult = RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);

        // Assert
        assertEquals(customerAddress1, actualResult);
    }

    @Test
    public void testGetLatestCustomerAddressNull() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createDate1= sdf.parse("2016-08-31 23:23:23");
        Date createDate2= sdf.parse("2016-09-31 3:23:23");
        Date createDate4= sdf.parse("2016-08-31 23:23:23");
        CustomerAddress customerAddress1 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Cooper", "Revenue Conduit", null, null,
                                    null, null, null, null, null, null, null, null, null, createDate2);
        CustomerAddress customerAddress2 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Copper", "Revenue Conduit", "526 S Main",
                                    "Akron Business Accelerator", "Akron", "ohio", "123", "1", "office", "44325",
                                    "1234567809", null, createDate1, createDate4);

        // Act
        CustomerAddress actualResult = RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);

        // Assert
        assertEquals(customerAddress2, actualResult);
    }

    @Test
    public void testGetLatestCustomerAddressNull2() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createDate1= sdf.parse("2016-08-31 23:23:23");
        Date createDate2= sdf.parse("2016-09-31 3:23:23");
        Date createDate4= sdf.parse("2016-08-31 23:23:23");
        CustomerAddress customerAddress1 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Cooper", "Revenue Conduit", null, null,
                                    null, null, null, null, null, null, null, null, createDate1, createDate2);
        CustomerAddress customerAddress2 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Copper", "Revenue Conduit", "526 S Main",
                                    "Akron Business Accelerator", "Akron", "ohio", "123", "1", "office", "44325",
                                    "1234567809", null, null, createDate4);

        // Act
        CustomerAddress actualResult = RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);

        // Assert
        assertEquals(customerAddress1, actualResult);
    }

    @Test
    public void testGetLatestCustomerAddressNullTest() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createDate1= sdf.parse("2016-08-31 23:23:23");
        Date createDate2= sdf.parse("2016-09-31 3:23:23");
        Date createDate4= sdf.parse("2016-08-31 23:23:23");
        CustomerAddress customerAddress1 = null;
        CustomerAddress customerAddress2 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Copper", "Revenue Conduit", "526 S Main",
                                    "Akron Business Accelerator", "Akron", "ohio", "123", "1", "office", "44325",
                                    "1234567809", null, null, createDate4);

        // Act
        CustomerAddress actualResult = RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);

        // Assert
        assertEquals(customerAddress2, actualResult);
    }

    @Test
    public void testGetLatestCustomerAddressNullTest2() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createDate1= sdf.parse("2016-08-31 23:23:23");
        Date createDate2= sdf.parse("2016-09-31 3:23:23");
        Date createDate4= sdf.parse("2016-08-31 23:23:23");
        CustomerAddress customerAddress1 =
                new CustomerAddress(1000, 1, new BigInteger("123"), "Brett", "Copper", "Revenue Conduit", "526 S Main",
                                    "Akron Business Accelerator", "Akron", "ohio", "123", "1", "office", "44325",
                                    "1234567809", null, null, createDate4);
        CustomerAddress customerAddress2 = null;

        // Act
        CustomerAddress actualResult = RFMHelpers.GetLatestCustomerAddress(customerAddress1, customerAddress2);

        // Assert
        assertEquals(customerAddress1, actualResult);
    }

    @Test
    public void testGetLatestCustomer() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createdAt= sdf.parse("2016-08-01 23:23:23");
        Date updatedAt= sdf.parse("2016-08-02 03:23:23");
        Date created= sdf.parse("2016-08-02 04:23:23");
        Date modified= sdf.parse("2016-08-02 06:23:23");
        Date createdAt1= sdf.parse("2016-07-01 22:23:23");
        Date updatedAt1= sdf.parse("2016-08-02 03:23:23");
        Date created1= sdf.parse("2016-08-02 00:23:23");
        Date modified1= sdf.parse("2016-08-02 06:23:23");
        Customer customer1= new Customer(new BigInteger("123"),234, new BigInteger("3") ,"Revenue", "Brett", "Cooper", "brett@rc.com", true, createdAt, updatedAt, false, created, modified, 124, true);
        Customer customer2= new Customer(new BigInteger("124"),235, new BigInteger("4") ,"Revenue", "Pika", "Chu", "pikachu@rc.com", true, createdAt1, updatedAt1, false, created1, modified1, 125, true);

        // Act
        Customer actualCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);

        // Assert
        assertEquals(customer1, actualCustomer);
    }

    @Test
    public void testGetLatestCustomerCaseIfElse() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createdAt= sdf.parse("2016-08-01 23:23:23");
        Date updatedAt= sdf.parse("2016-08-02 03:23:23");
        Date created= sdf.parse("2016-08-02 04:23:23");
        Date modified= sdf.parse("2016-08-02 06:23:23");
        Date createdAt1= sdf.parse("2016-07-01 22:23:23");
        Date updatedAt1= sdf.parse("2016-08-02 03:23:23");
        Date created1= sdf.parse("2016-08-02 05:23:23");
        Date modified1= sdf.parse("2016-08-02 06:23:23");
        Customer customer1= new Customer(new BigInteger("123"),234, new BigInteger("3") ,"Revenue", "Brett", "Cooper", "brett@rc.com", true, createdAt, updatedAt, false, created, modified, 124, true);
        Customer customer2= new Customer(new BigInteger("124"),235, new BigInteger("4") ,"Revenue", "Pika", "Chu", "pikachu@rc.com", true, createdAt1, updatedAt1, false, created1, modified1, 125, true);

        // Act
        Customer actualCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);

        // Assert
        assertEquals(customer2, actualCustomer);
    }

    @Test
    public void testGetLatestCustomerCaseElseIf() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createdAt= sdf.parse("2016-08-01 23:23:23");
        Date updatedAt= sdf.parse("2016-08-02 03:23:23");
        Date created= sdf.parse("2016-08-02 04:23:23");
        Date modified= sdf.parse("2016-08-02 06:23:23");
        Date createdAt1= sdf.parse("2016-07-01 22:23:23");
        Date updatedAt1= sdf.parse("2016-08-02 03:23:23");
        Date created1= null;
        Date modified1= sdf.parse("2016-08-02 06:23:23");
        Customer customer1= new Customer(new BigInteger("123"),234, new BigInteger("3") ,"Revenue", "Brett", "Cooper", "brett@rc.com", true, createdAt, updatedAt, false, created, modified, 124, true);
        Customer customer2= new Customer(new BigInteger("124"),235, new BigInteger("4") ,"Revenue", "Pika", "Chu", "pikachu@rc.com", true, createdAt1, updatedAt1, false, created1, modified1, 125, true);

        // Act
        Customer actualCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);

        // Assert
        assertEquals(customer1, actualCustomer);
    }
    @Test
    public void testGetLatestCustomerCaseElse() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createdAt = sdf.parse("2016-08-01 23:23:23");
        Date updatedAt = sdf.parse("2016-08-02 03:23:23");
        Date created = null;
        Date modified = sdf.parse("2016-08-02 06:23:23");
        Date createdAt1 = sdf.parse("2016-07-01 22:23:23");
        Date updatedAt1 = sdf.parse("2016-08-02 03:23:23");
        Date created1 = sdf.parse("2016-08-02 04:23:23");
        Date modified1 = sdf.parse("2016-08-02 06:23:23");
        Customer customer1 = new Customer(new BigInteger("123"), 234, new BigInteger("3"), "Revenue", "Brett", "Cooper", "brett@rc.com", true, createdAt, updatedAt, false, created, modified, 124, true);
        Customer customer2 = new Customer(new BigInteger("124"), 235, new BigInteger("4"), "Revenue", "Pika", "Chu", "pikachu@rc.com", true, createdAt1, updatedAt1, false, created1, modified1, 125, true);

        // Act
        Customer actualCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);

        // Assert
        assertEquals(customer2, actualCustomer);
    }

    @Test
    public void testGetLatestCustomerNull() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date createdAt= sdf.parse("2016-08-01 23:23:23");
        Date updatedAt= sdf.parse("2016-08-02 03:23:23");
        Date created= null;
        Date modified= sdf.parse("2016-08-02 06:23:23");
        Date createdAt1= sdf.parse("2016-07-01 22:23:23");
        Date updatedAt1= sdf.parse("2016-08-02 03:23:23");
        Date created1= null;
        Date modified1= sdf.parse("2016-08-02 06:23:23");
        Customer customer1= new Customer(new BigInteger("123"),234, new BigInteger("3") ,"Revenue", "Brett", "Cooper", "brett@rc.com", true, createdAt, updatedAt, false, created, modified, 124, true);
        Customer customer2= new Customer(new BigInteger("124"),235, new BigInteger("4") ,"Revenue", "Pika", "Chu", "pikachu@rc.com", true, createdAt1, updatedAt1, false, created1, modified1, 125, true);

        // Act
        Customer actualCustomer = RFMHelpers.GetLatestCustomer(customer1, customer2);

        // Assert
        assertEquals(customer2, actualCustomer);
    }

    @Test
    public void testRemoveEscapeSequences() throws Exception {
        // Arrange
        String str = "This is a test\\| application\\| for removing\\| Escape Seq";
        String expectedResult = "This is a test application for removing Escape Seq";

        // Act
        String actualResult = RFMHelpers.RemoveEscapeSequences(str, RFMCalc.DELIMITER);

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void removeCustomerAddressEscapeSequencesTest() {
        String customerAddress =
                "1000|10551297|7663407|2810 Shoal creek Rd N.W.  Monroe, Ga. 30656|Dewberry|Southern ELITE Contracting INC|2810,Shoal C\\|reek Rd NW|null|Monroe\\,|Georgia|United States|GA|US|shipping|30656|7707693789|2017-01-04 17:56:50|2017-01-04 17:56:50";
        String expectedCustomerAddress =
                "1000|10551297|7663407|2810 Shoal creek Rd N.W.  Monroe, Ga. 30656|Dewberry|Southern ELITE Contracting INC|2810,Shoal Creek Rd NW|null|Monroe\\,|Georgia|United States|GA|US|shipping|30656|7707693789|2017-01-04 17:56:50|2017-01-04 17:56:50";

        // Act
        String actualResult = RFMHelpers.RemoveEscapeSequences(customerAddress, RFMCalc.DELIMITER);
        String[] strings = actualResult.split("\\" + RFMCalc.DELIMITER);

        // Assert
        assertEquals(18, strings.length);
        assertEquals(expectedCustomerAddress, actualResult);
    }

    @Test
    public void customerSplitTest() {
        String customer =
                "2158151|1000|3483|null|Jonathan|Bates|jbates@mweb.co.za|false|2015-01-14 08:10:39|2015-01-14 08:10:39|true|2015-12-15 17:03:37|2015-12-15 17:03:37|null|null";

        String[] strings = customer.split("\\" + RFMCalc.DELIMITER);

        // Assert
        assertEquals(15, strings.length);
    }

    @Test
    public void testTryParseBigDecimal() throws Exception {
        // Arrange
        BigDecimal bigDecimal = new BigDecimal("123.00");

        // Act
        BigDecimal actualResult = RFMHelpers.TryParseBigDecimal(bigDecimal.toString());

        // Assert
        assertEquals(bigDecimal, actualResult);
    }

    @Test
    public void testTryParseBigDecimalNull() throws Exception {
        // Arrange
        String nullString = new String("null");

        // Act
        BigDecimal actualResult = RFMHelpers.TryParseBigDecimal(nullString);

        // Assert
        assertEquals(null, actualResult);
    }

    @Test
    public void testTryParseBigDecimalEmpty() throws Exception {
        // Arrange
        String nullString = new String();

        // Act
        BigDecimal actualResult = RFMHelpers.TryParseBigDecimal(nullString);

        // Assert
        assertEquals(null, actualResult);
    }

    @Test
    public void testTryParseInteger() throws Exception {
        // Arrange
        Integer integer = 123;

        // Act
        Integer actualResult = RFMHelpers.TryParseInteger(integer.toString());

        // Assert
        assertEquals(integer, actualResult);
    }

    @Test
    public void testTryParseIntegerEmpty() throws Exception {
        // Arrange
        String nullString = new String();

        // Act
        Integer actualResult = RFMHelpers.TryParseInteger(nullString);
        // Assert

        assertEquals(null, actualResult);
    }

    @Test
    public void testTryParseIntegerNull() throws Exception {
        // Arrange
        String nullString = new String("null");

        // Act
        Integer actualResult = RFMHelpers.TryParseInteger(nullString);

        // Assert
        assertEquals(null, actualResult);
    }

    @Test
    public void testTryParseBigInteger() throws Exception {
        // Arrange
        BigInteger bigInteger = new BigInteger("123");

        // Act
        BigInteger actualResult = RFMHelpers.TryParseBigInteger(bigInteger.toString());

        // Assert
        assertEquals(bigInteger, actualResult);
    }

    @Test
    public void testTryParseBigIntegerEmpty() throws Exception {
        // Arrange
        String nullString = new String();

        // Act
        BigInteger actualResult = RFMHelpers.TryParseBigInteger(nullString);

        // Assert
        assertEquals(null, actualResult);
    }

    @Test
    public void testTryParseBigIntegerNull() throws Exception {
        // Arrange
        BigInteger bigInteger = null;
        String nullString = new String("null");

        // Act
        BigInteger actualResult = RFMHelpers.TryParseBigInteger(nullString);

        // Assert
        assertEquals(null, actualResult);
    }

    @Test
   public void testTryParseBooleanTrue() throws Exception {
        // Arrange
        String trueString = "True";
        boolean expectedResult = true;

        // Act
        Boolean actualResult = RFMHelpers.TryParseBoolean(trueString);

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testTryParseBooleanCaseInsensitive() throws Exception {
        // Arrange
        String trueString = "true";
        boolean expectedResult = true;

        // Act
        Boolean actualResult = RFMHelpers.TryParseBoolean(trueString);

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testTryParseBooleanFalse() throws Exception {
        // Arrange
        String trueString = null;
        Boolean expectedResult = null;

        // Act
        Boolean actualResult = RFMHelpers.TryParseBoolean(trueString);

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testTryParseBooleanNullString() throws Exception {
        // Arrange
        String trueString = "null";
        Boolean expectedResult = null;

        // Act
        Boolean actualResult = RFMHelpers.TryParseBoolean(trueString);

        // Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testTryParseDate() throws Exception {
        // Arrange
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date expectedDate = sdf.parse("2016-10-10 10:10:10");

        // Act
        Date actualDate = RFMHelpers.TryParseDate("2016-10-10 10:10:10", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        // Assert

        assertEquals(expectedDate,actualDate);
    }

    @Test
    public void testTryParseDateNull() throws Exception {
        // Arrange
        Date expectedDate = null;

        // Act
        Date actualDate = RFMHelpers.TryParseDate("null", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        // Assert
        assertEquals(expectedDate, actualDate);
    }

    @Test
    public void testTryParseDateEmpty() throws Exception {
        // Arrange
        Date expectedDate = null;

        // Act
        Date actualDate = RFMHelpers.TryParseDate("", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

        // Assert
        assertEquals(expectedDate, actualDate);
    }
}